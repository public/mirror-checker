package checkers

import (
	"regexp"
	"strings"
	"time"
)

var GentooPortageProject Project = Project{
	Name:          "gentooportage",
	Properties:    DefaultProjectProperties,
	NumOfCheckers: 1,
	Checkers: []*ProjectChecker{
		GetDefaultChecker("gentooportage", true, func(*Project) (bool, error) {
			// config sanity check
			data := EnabledProjects["gentooportage"].Properties
			// TODO: assert
			err := AssertStrings()
			if err != nil {
				return false, GetError(err, "GentooPortage", "config sanity check")
			}

			// SOURCE: https://git.csclub.uwaterloo.ca/public/mirror-checker/src/branch/master/projects/gentooportage.py

			upstream_body, err := httpGET(data.Upstream)
			if err != nil {
				return false, GetError(err, "GentooPortage", "getting upstream file")
			}
			page := string(upstream_body)

			indexOfFile := strings.Index(page, "rsync4.ca.gentoo.org")
			if indexOfFile == -1 {
				return false, GetError(nil, "GentooPortage", "no index of file")
			}

			re := regexp.MustCompile(`(\d+ minutes?)|(\d+ hours?)|(\d+(\.)?\d+ days?)`)
			m := re.FindStringSubmatch(page[indexOfFile:])
			if len(m) == 0 || len(m[0]) == 0 {
				return false, GetError(nil, "GentooPortage", "no matches for regex in file")
			}

			// fmt.Println(m[0])
			// eg. duration: "20 minutes"
			duration, err := getTimeDelta(m[0])
			if err != nil {
				return false, GetError(err, "GentooPortage", "parsing duration")
			}

			// Return whether the duration is less than or equal to the out_of_sync_interval
			return (duration <= time.Duration(data.OOSInterval)*time.Second), nil
		}),
	},
}
