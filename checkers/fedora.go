package checkers

import (
	"time"

	"git.csclub.uwaterloo.ca/public/mirror-checker/config"
)

var FedoraProject Project = Project{
	Name:          "fedora",
	Properties:    DefaultProjectProperties,
	NumOfCheckers: 1,
	Checkers: []*ProjectChecker{
		GetDefaultChecker("fedora", true, func(*Project) (bool, error) {
			// config sanity check
			data := EnabledProjects["fedora"].Properties
			err := AssertStrings(config.MirrorBaseURL, data.CSC, data.File, data.Upstream)
			if err != nil {
				return false, GetError(err, "Fedora", "config sanity check")
			}

			// SOURCE: https://git.csclub.uwaterloo.ca/public/mirror-checker/src/branch/master/projects/fedora.py

			csc_url := config.MirrorBaseURL + data.CSC + data.File
			upstream_url := data.Upstream + data.File

			csc_body, err := httpGET(csc_url)
			if err != nil {
				return false, GetError(err, "Fedora", "getting CSC file")
			}
			upstream_body, err := httpGET(upstream_url)
			if err != nil {
				return false, GetError(err, "Fedora", "getting upstream file")
			}

			CSC := string(csc_body)
			upstream := string(upstream_body)

			if CSC == upstream {
				return true, nil
			}

			// # Date example: Fedora-Rawhide-20220725.n.1
			data_format := "20060102"

			CSC_date, err := time.Parse(data_format, CSC[15:23])
			if err != nil {
				return false, GetError(err, "Fedora", "parsing CSC date")
			}
			CSC_utc_time := CSC_date.Unix()

			upstream_date, err := time.Parse(data_format, upstream[15:23])
			if err != nil {
				return false, GetError(err, "Fedora", "parsing upstream date")
			}
			upstream_utc_time := upstream_date.Unix()

			delta := (upstream_utc_time - CSC_utc_time)

			return (delta < data.OOSInterval && delta > -data.OOSInterval), nil
		}),
	},
}
