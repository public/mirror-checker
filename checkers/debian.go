package checkers

import (
	"strings"
	"time"

	"git.csclub.uwaterloo.ca/public/mirror-checker/config"
)

var DebianProject Project = Project{
	Name:          "debian",
	Properties:    DefaultProjectProperties,
	NumOfCheckers: 1,
	Checkers: []*ProjectChecker{
		GetDefaultChecker("debian", true, func(*Project) (bool, error) {
			// config sanity check
			data := EnabledProjects["debian"].Properties
			err := AssertStrings(config.MirrorBaseURL, data.Upstream, data.File)
			if err != nil {
				return false, GetError(err, "Debian", "config sanity check")
			}

			// Modelled after: https://git.csclub.uwaterloo.ca/public/mirror-checker/src/branch/master/projects/debian.py
			// NOTE: cloned in debiancd, debianmultimedia, debianports, debiansecurity

			csc_url := config.MirrorBaseURL + data.CSC + data.File
			upstream_url := data.Upstream + data.File

			// make HTTP GET request to csc_url
			csc_body, err := httpGET(csc_url)
			if err != nil {
				return false, GetError(err, "Debian", "getting CSC file")
			}

			// make HTTP GET request to upstream_url
			upstream_body, err := httpGET(upstream_url)
			if err != nil {
				return false, GetError(err, "Debian", "getting upstream file")
			}

			// parse bodies as string
			CSC := string(csc_body)
			upstream := string(upstream_body)

			// same date stamp
			if CSC == upstream {
				return true, nil
			}

			// check if delta is within threshold
			// equiv to: "%a %b %d %H:%M:%S UTC %Y"
			date_format := "Mon Jan 2 15:04:05 MST 2006"

			CSC_date, err := time.Parse(date_format, strings.Split(CSC, "\n")[0])
			if err != nil {
				return false, GetError(err, "Debian", "parsing CSC date")
			}
			CSC_utc_time := CSC_date.Unix()

			upstream_date, err := time.Parse(date_format, strings.Split(upstream, "\n")[0])
			if err != nil {
				return false, GetError(err, "Debian", "parsing upstream date")
			}
			upstream_utc_time := upstream_date.Unix()

			delta := (upstream_utc_time - CSC_utc_time)

			return (delta < data.OOSInterval && delta > -data.OOSInterval), nil
		}),
	},
}
