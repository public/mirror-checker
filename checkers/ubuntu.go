package checkers

import (
	"strings"

	"github.com/rs/zerolog/log"
)

var UbuntuProject Project = Project{
	Name:          "ubuntu",
	Properties:    DefaultProjectProperties,
	NumOfCheckers: 1,
	Checkers: []*ProjectChecker{
		GetDefaultChecker("ubuntu", true, func(*Project) (bool, error) {
			data := EnabledProjects["ubuntu"].Properties
			err := AssertStrings(data.Upstream)
			if err != nil {
				return false, GetError(err, "Ubuntu", "config sanity check")
			}

			// SOURCE: https://git.csclub.uwaterloo.ca/public/mirror-checker/src/branch/master/projects/ubuntu.py
			res, err := httpGET(data.Upstream)
			if err != nil {
				return false, GetError(err, "Ubuntu", "getting upstream data")
			}
			page := string(res)

			// count occurences of "Up to date"
			count := strings.Count(page, "Up to date")

			NUM_UBUNTU_RELEASES := 24 // TODO: should be updated automatically (from another source)
			THRESHOLD := 5            // it would be pretty bad if we don't update this checker for 5 (major) ubuntu releases

			log.Debug().Str("project", "Ubuntu").Int("count", count).Msg("counted occurences of 'Up to date'")

			return count >= NUM_UBUNTU_RELEASES && count < NUM_UBUNTU_RELEASES+THRESHOLD, nil
		}),
	},
}
