package checkers

import (
	"strconv"
	"strings"

	"git.csclub.uwaterloo.ca/public/mirror-checker/config"
)

var GentooDistProject Project = Project{
	Name:          "gentoodistfiles",
	Properties:    DefaultProjectProperties,
	NumOfCheckers: 1,
	Checkers: []*ProjectChecker{
		GetDefaultChecker("gentoodistfiles", true, func(*Project) (bool, error) {
			// config sanity check
			data := EnabledProjects["gentoodistfiles"].Properties

			// SOURCE: https://git.csclub.uwaterloo.ca/public/mirror-checker/src/branch/master/projects/gentoodistfiles.py

			// 	csc_url = CSC_MIRROR + data[project]["csc"] + data[project]["file"]
			// upstream_url = data[project]["upstream"] + data[project]["file"]
			csc_url := config.MirrorBaseURL + data.CSC + data.File
			upstream_url := data.Upstream + data.File

			// req = requests.get(csc_url)
			// req.raise_for_status()
			// CSC = req.text
			csc_body, err := httpGET(csc_url)
			if err != nil {
				return false, GetError(err, "GentooDistFiles", "getting CSC file")
			}

			upstream_body, err := httpGET(upstream_url)
			if err != nil {
				return false, GetError(err, "GentooDistFiles", "getting upstream file")
			}

			CSC := string(csc_body)
			upstream := string(upstream_body)

			if CSC == upstream {
				return true, nil
			}

			// parse time as int
			CSC_utc_time, err := strconv.ParseInt(strings.TrimSpace(CSC[0:11]), 10, 64)
			if err != nil {
				return false, GetError(err, "GentooDistFiles", "parsing CSC date")
			}
			upstream_utc_time, err := strconv.ParseInt(strings.TrimSpace(upstream[0:11]), 10, 64)
			if err != nil {
				return false, GetError(err, "GentooDistFiles", "parsing upstream date")
			}

			delta := (upstream_utc_time - CSC_utc_time)

			return (delta < data.OOSInterval && delta > -data.OOSInterval), nil
		}),
	},
}
