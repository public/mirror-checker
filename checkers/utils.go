package checkers

import (
	"errors"
	"fmt"
	"io"
	"net/http"
	"strconv"
	"strings"
	"time"

	"github.com/rs/zerolog/log"
)

// duration parser
func getTimeDelta(input string) (time.Duration, error) {
	input = strings.TrimSpace(input)
	parts := strings.Split(input, " ")

	number, err := strconv.Atoi(parts[0])
	if err != nil {
		return 0, err
	}

	switch parts[1] {
	case "minute", "minutes":
		return time.Duration(number) * time.Minute, nil
	case "hour", "hours":
		return time.Duration(number) * time.Hour, nil
	case "day", "days":
		return time.Duration(number) * 24 * time.Hour, nil
	default:
		return 0, fmt.Errorf("unknown time unit")
	}
}

// http helpers
func httpGET(url string) ([]byte, error) {
	res, err := http.Get(url)

	startTime := time.Now()

	if err != nil {
		log.Error().Msgf("Error making GET request: %s", err.Error())
		return nil, err
	}
	defer res.Body.Close()

	body, err := io.ReadAll(res.Body)
	if err != nil {
		log.Error().Msgf("Error reading response body: %s", err.Error())
		return nil, err
	}

	log.Debug().
		Dur("time", time.Since(startTime)).
		Str("status", res.Status).
		Msgf("GET %s", url)

	return body, nil
}

// error parser
func GetError(err error, project string, msg string) error {
	if err == nil {
		return errors.New("<---")
	}

	// throw given error while denoating project name and description
	errMsg := fmt.Sprintf("%s: '%s' received error '%s'", project, msg, err.Error())

	return errors.New(errMsg)
}
