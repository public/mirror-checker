package checkers

import (
	"strings"
	"time"

	"git.csclub.uwaterloo.ca/public/mirror-checker/config"
)

var DebianMultimediaProject Project = Project{
	Name:          "debianmultimedia",
	Properties:    DefaultProjectProperties,
	NumOfCheckers: 1,
	Checkers: []*ProjectChecker{
		GetDefaultChecker("debianmultimedia", true, func(*Project) (bool, error) {
			// based on debian.go checker
			// config sanity check
			data := EnabledProjects["debianmultimedia"].Properties
			err := AssertStrings(config.MirrorBaseURL, data.Upstream, data.File)
			if err != nil {
				return false, GetError(err, "DebianMultimedia", "config sanity check")
			}

			csc_url := config.MirrorBaseURL + data.CSC + data.File
			upstream_url := data.Upstream + data.File

			csc_body, err := httpGET(csc_url)
			if err != nil {
				return false, GetError(err, "DebianMultimedia", "getting CSC file")
			}
			upstream_body, err := httpGET(upstream_url)
			if err != nil {
				return false, GetError(err, "DebianMultimedia", "getting upstream file")
			}

			CSC := string(csc_body)
			upstream := string(upstream_body)

			if CSC == upstream {
				return true, nil
			}

			date_format := "Mon Jan 2 15:04:05 MST 2006"

			CSC_date, err := time.Parse(date_format, strings.Split(CSC, "\n")[0])
			if err != nil {
				return false, GetError(err, "DebianMultimedia", "parsing CSC date")
			}
			CSC_utc_time := CSC_date.Unix()

			upstream_date, err := time.Parse(date_format, strings.Split(upstream, "\n")[0])
			if err != nil {
				return false, GetError(err, "DebianMultimedia", "parsing upstream date")
			}
			upstream_utc_time := upstream_date.Unix()

			delta := (upstream_utc_time - CSC_utc_time)

			return (delta < data.OOSInterval && delta > -data.OOSInterval), nil
		}),
	},
}
