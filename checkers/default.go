package checkers

import (
	"fmt"
	"time"

	"github.com/rs/zerolog/log"
)

////////////////////////////////////////////////////////////////////////////////

var DefaultProjectProperties = ProjectProperties{
	OOSSince:    time.Now(),
	OOSInterval: 3600,
	CSC:         "unknown",
	Upstream:    "unknown",
	File:        "unknown",
}

var DefaultCheckerResult CheckerResult = CheckerResult{
	ProjectName: "unknown",
	CheckerName: "unknown",
	Status:      CHECKER_PROGRESS,
}

////////////////////////////////////////////////////////////////////////////////

// All "projects" which have been implemented.
var SupportedProjects map[string]*Project = map[string]*Project{
	// "almalinux":&AlmaLinuxProject,
	// "alpine":&AlpineProject,
	// "apache":&ApacheProject,
	// "arch":&ArchProject,
	// "artix":&ArtixProject,
	// "centos":&CentosProject,
	// "ceph":&CephProject,
	// "cpan":&CpanProject,
	// "cran":&CranProject,
	// "ctan":&CtanProject,
	// "cygwin":&CygwinProject,
	"debian":           &DebianProject,
	"debiancd":         &DebianCDProject,
	"debianmultimedia": &DebianMultimediaProject,
	"debianports":      &DebianPortsProject,
	"debiansecurity":   &DebianSecurityProject,
	// "eclipse":&EclipseProject,
	"fedora": &FedoraProject,
	// "freebsd":&FreebsdProject,
	"gentoodistfiles": &GentooDistProject,
	"gentooportage":   &GentooPortageProject,
	// "gnome":&GnomeProject,
	// "gnu":&GnuProject,
	// "gutenberg":&GutenbergProject,
	// "ipfire":&IpfireProject,
	// "kde":&KdeProject,
	// "kdeapplicationdata":&KdeApplicationDataProject,
	// "kernel":&KernelProject,
	// "linuxmint":&LinuxMintProject,
	// "linuxmint_packages":&LinuxMintPackagesProject,
	// "macports":&MacPortsProject,
	"manjaro": &ManjaroProject,
	// "mxlinux":&MxLinuxProject,
	// "mxlinux_iso":&MxLinuxIsoProject,
	// "mysql":&MysqlProject,
	// "netbsd":&NetBsdProject,
	// "nongnu":&NongnuProject,
	// "openbsd":&OpenbsdProject,
	// "opensuse":&OpensuseProject,
	// "parabola":&ParabolaProject,
	// "pkgsrc":&PkgsrcProject,
	// "puppy_linux":&PuppyLinuxProject,
	// "qtproject":&QtProject,
	// "racket":&RacketProject,
	// "raspberrypi":&RaspberrypiProject,
	// "raspbian":&RaspbianProject,
	// "sage":&SageProject,
	// "saltstack":&SaltStackProject,
	// "slackware":&SlackwareProject,
	// "tdf":&TdfProject,
	// "trisquel":&TrisquelProject,
	"ubuntu": &UbuntuProject,
	// "ubuntu_ports":&UbuntuPortsProject,
	// "ubuntu_ports_releases":&UbuntuPortsReleasesProject,
	// "ubuntu_releases":&UbuntuReleasesProject,
	// "vlc":&VlcProject,
	// "x_org":&XorgProject,
	// "xiph":&XiphProject,
	// "xubuntu_releases":&XubuntuReleasesProject,
}

func LoadDefaultProjects() {
	// Load all projects that's implemented

	log.Info().Msg("Loading Default Projects.")

	// LoadProject("almalinux")
	// LoadProject("alpine")
	// LoadProject("apache")
	// LoadProject("arch")
	// LoadProject("artix")
	// LoadProject("centos")
	// LoadProject("ceph")
	// LoadProject("cpan")
	// LoadProject("cran")
	// LoadProject("ctan")
	// LoadProject("cygwin")
	LoadProject("debian")
	LoadProject("debiancd")
	LoadProject("debianmultimedia")
	LoadProject("debianports")
	LoadProject("debiansecurity")
	// LoadProject("eclipse")
	LoadProject("fedora")
	// LoadProject("freebsd")
	// LoadProject("gentoodistfiles")
	// LoadProject("gentooportage")
	// LoadProject("gnome")
	// LoadProject("gnu")
	// LoadProject("gutenberg")
	// LoadProject("ipfire")
	// LoadProject("kde")
	// LoadProject("kdeapplicationdata")
	// LoadProject("kernel")
	// LoadProject("linuxmint")
	// LoadProject("linuxmint_packages")
	// LoadProject("macports")
	// LoadProject("manjaro")
	// LoadProject("mxlinux")
	// LoadProject("mxlinux_iso")
	// LoadProject("mysql")
	// LoadProject("netbsd")
	// LoadProject("nongnu")
	// LoadProject("openbsd")
	// LoadProject("opensuse")
	// LoadProject("parabola")
	// LoadProject("pkgsrc")
	// LoadProject("puppy_linux")
	// LoadProject("qtproject")
	// LoadProject("racket")
	// LoadProject("raspberrypi")
	// LoadProject("raspbian")
	// LoadProject("sage")
	// LoadProject("saltstack")
	// LoadProject("slackware")
	// LoadProject("tdf")
	// LoadProject("trisquel")
	LoadProject("ubuntu")
	// LoadProject("ubuntu_ports")
	// LoadProject("ubuntu_ports_releases")
	// LoadProject("ubuntu_releases")
	// LoadProject("vlc")
	// LoadProject("x_org")
	// LoadProject("xiph")
	// LoadProject("xubuntu_releases")

}

func AssertStrings(s ...string) error {
	for i, str := range s {
		if str == "" {
			return fmt.Errorf("failed assert: string %d of %d is empty", i+1, len(s))
		}
	}
	return nil
}

// TODO: add checker logger
func GetDefaultChecker(name string, def bool, f func(*Project) (bool, error)) *ProjectChecker {
	checkerName := name
	if def {
		checkerName += "_default"
	}

	// configure checker function
	var checkerFunc func() (bool, error)
	if f != nil {
		// has custom checker function
		checkerFunc = func() (bool, error) {
			proj, err := GetProject(name)
			if err != nil {
				return false, err
			}
			return f(proj)
		}
	} else {
		// no custom checker function
		checkerFunc = func() (bool, error) {
			return false, fmt.Errorf("project '%s' not supported", name)
		}
	}

	checker := ProjectChecker{
		Name:         checkerName,
		CheckProject: checkerFunc,
		Default:      def,
	}

	return &checker
}
