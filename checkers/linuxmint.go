package checkers

import (
	"regexp"
	"strconv"
	"strings"
	"time"
)

var LinuxMintProject Project = Project{
	Name:          "linuxmint",
	Properties:    DefaultProjectProperties,
	NumOfCheckers: 1,
	Checkers: []*ProjectChecker{
		GetDefaultChecker("linuxmint", true, func(*Project) (bool, error) {
			data := EnabledProjects["linuxmint"].Properties
			err := AssertStrings(data.Upstream)

			if err != nil {
				return false, GetError(err, "Linux Mint", "config sanity check")
			}
			upstream_body, err := httpGET(data.Upstream)
			if err != nil {
				return false, GetError(err, "Linux Mint", "getting upstream file")
			}
			page := string(upstream_body)
			indexOfFile := strings.Index(page, "mirror.csclub.uwaterloo.ca/linuxmint")
			if indexOfFile == -1 {
				return false, GetError(nil, "Linux Mint", "no index of file")
			}
			re := regexp.MustCompile(`(?P<hours>\d+):(?P<minutes>\d+)`)
			m := re.FindStringSubmatch(page[indexOfFile:])
			if len(m) == 0 || len(m[0]) == 0 {
				return false, GetError(nil, "Linux Mint", "no matches for regex in file")
			}
			split := strings.Split(m[0], ":")
			hours, err := strconv.Atoi(split[0])
			if err != nil {
				return false, GetError(err, "Linux Mint", "parsing hours")
			}
			minutes, err := strconv.Atoi(split[1])
			if err != nil {
				return false, GetError(err, "Linux Mint", "parsing minutes")
			}
			duration := time.Duration(hours)*time.Hour + time.Duration(minutes)*time.Minute
			return (duration <= time.Duration(data.OOSInterval)*time.Second), nil
		}),
	},
}
