package checkers

import "time"

// "Projects" which have valid configs.
var EnabledProjects = make(map[string]*Project)

// TODO: refactor all errors into variables

////////////////////////////////////////////////////////////////////////////////
// Project, Project Properties, Project Checker

type Project struct {
	Name          string
	Properties    ProjectProperties
	NumOfCheckers int
	Checkers      []*ProjectChecker
}

type ProjectProperties struct {
	OOSSince    time.Time //`json:"out_of_sync_since"`
	OOSInterval int64     `json:"out_of_sync_interval"` // in seconds
	CSC         string    `json:"csc"`
	Mirrors     []string  `json:"mirrors"`
	Upstream    string    `json:"upstream"`
	File        string    `json:"file"`
}

type ProjectChecker struct {
	Name         string `json:"name"`
	CheckProject CPFunc
	Default      bool `json:"default"`
	// TODO: other severity levels?
}
type CPFunc func() (bool, error)

// //////////////////////////////////////////////////////////////////////////////
// Checker Status and Checker Result
type CheckerStatus string

var CHECKER_SUCCESS CheckerStatus = "success"
var CHECKER_PROGRESS CheckerStatus = "progress"
var CHECKER_ERROR CheckerStatus = "error"
var CHECKER_FAIL CheckerStatus = "fail"

type CheckerResult struct {
	ProjectName string        `json:"project_name"`
	CheckerName string        `json:"checker_name"`
	Time        time.Time     `json:"start_time"`
	EndTime     time.Time     `json:"end_time"`
	Status      CheckerStatus `json:"status"`
	Error       error         `json:"error"`
}

type CheckerResultCallback func(CheckerResult)
