package checkers

import (
	"encoding/json"
	"os"

	"git.csclub.uwaterloo.ca/public/mirror-checker/config"
	"github.com/rs/zerolog/log"
)

type MirrorData map[string]ProjectProperties

func LoadFromFile(path string) error {
	raw_data, err := os.ReadFile(path)
	if err != nil {
		return err
	}

	var data MirrorData
	err = json.Unmarshal(raw_data, &data)
	if err != nil {
		return err
	}

	// TODO: load in alphabetical order
	// Access the parsed data
	for proj, prop := range data {
		log.Info().Str("project", proj).Msg("Enabled Project.")

		sp, exists := SupportedProjects[config.NormalizeName(proj)]
		if !exists {
			log.Warn().Str("project", proj).Msg("Project not supported.")
			continue
		}

		sp.Properties = prop

		log.Debug().
			Str("distribution", proj).
			// Time("out_of_sync_since", prop.OOSSince).
			Int64("out_of_sync_interval", prop.OOSInterval).
			Str("csc", prop.CSC).
			Str("upstream", prop.Upstream).
			Str("file", prop.File).
			Msg("Loaded project config.")
	}

	return nil
}
