package checkers

import (
	"regexp"
	"strconv"
	"strings"
	"time"
)

var RaspberryPiProject Project = Project{
	Name:          "raspberrypi",
	Properties:    DefaultProjectProperties,
	NumOfCheckers: 1,
	Checkers: []*ProjectChecker{
		GetDefaultChecker("raspberrypi", true, func(*Project) (bool, error) {
			data := EnabledProjects["raspberrypi"].Properties
			err := AssertStrings(data.Upstream)

			if err != nil {
				return false, GetError(err, "Raspberry Pi", "config sanity check")
			}
			upstream_body, err := httpGET(data.Upstream)
			if err != nil {
				return false, GetError(err, "Raspberry Pi", "getting upstream file")
			}
			page := string(upstream_body)
			indexOfFile := strings.Index(page, "archive.raspberrypi.org")
			if indexOfFile == -1 {
				return false, GetError(nil, "Raspberry Pi", "no index of file")
			}
			re := regexp.MustCompile(`(?P<hours>\d+):(?P<minutes>\d+)`)
			m := re.FindStringSubmatch(page[indexOfFile:])
			if len(m) == 0 || len(m[0]) == 0 {
				return false, GetError(nil, "Raspberry Pi", "no matches for regex in file")
			}
			split := strings.Split(m[0], ":")
			hours, err := strconv.Atoi(split[0])
			if err != nil {
				return false, GetError(err, "Raspberry Pi", "parsing hours")
			}
			minutes, err := strconv.Atoi(split[1])
			if err != nil {
				return false, GetError(err, "Raspberry Pi", "parsing minutes")
			}
			duration := time.Duration(hours)*time.Hour + time.Duration(minutes)*time.Minute
			return (duration <= time.Duration(data.OOSInterval)*time.Second), nil
		}),
	},
}
