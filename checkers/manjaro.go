package checkers

import (
	"regexp"
	"strconv"
	"strings"
	"time"
)

var ManjaroProject Project = Project{
	Name:          "manjaro",
	Properties:    DefaultProjectProperties,
	NumOfCheckers: 1,
	Checkers: []*ProjectChecker{
		GetDefaultChecker("manjaro", true, func(*Project) (bool, error) {
			// config sanity check
			data := EnabledProjects["manjaro"].Properties
			// TODO: assert
			err := AssertStrings()
			if err != nil {
				return false, GetError(err, "Manjaro", "config sanity check")
			}

			// SOURCE: https://git.csclub.uwaterloo.ca/public/mirror-checker/src/branch/master/projects/manjaro.py

			upstream_body, err := httpGET(data.Upstream)
			if err != nil {
				return false, GetError(err, "Manjaro", "getting upstream file")
			}
			page := string(upstream_body)

			indexOfFile := strings.Index(page, "mirror.csclub.uwaterloo.ca/manjaro")
			if indexOfFile == -1 {
				return false, GetError(nil, "Manjaro", "no index of file")
			}

			re := regexp.MustCompile(`(?P<hours>\d+):(?P<minutes>\d+)`)
			m := re.FindStringSubmatch(page[indexOfFile:])
			if len(m) == 0 || len(m[0]) == 0 {
				return false, GetError(nil, "Manjaro", "no matches for regex in file")
			}

			// fmt.Println(m[0])
			// eg. 02:33
			split := strings.Split(m[0], ":")
			hours, err := strconv.Atoi(split[0])
			if err != nil {
				return false, GetError(err, "Manjaro", "parsing hours")
			}
			minutes, err := strconv.Atoi(split[1])
			if err != nil {
				return false, GetError(err, "Manjaro", "parsing minutes")
			}

			duration := time.Duration(hours)*time.Hour + time.Duration(minutes)*time.Minute

			return (duration <= time.Duration(data.OOSInterval)*time.Second), nil
		}),
	},
}
