package config

import (
	"time"
)

var MirrorBaseURL = "http://mirror.csclub.uwaterloo.ca/" // TODO: must start with https://

var loggingTimeFormat = time.RFC3339
