package config

import (
	"path/filepath"
	"regexp"
	"strings"

	"github.com/rs/zerolog/log"
)

func GetAbsPath(path string) string {
	absPath, err := filepath.Abs(path)
	if err != nil {
		log.Error().Err(err).Msg("Failed to get absolute path.")
		return path
	}
	return absPath
}

var lowerAlphaRegex = regexp.MustCompile("[^a-z]+")

func NormalizeName(name string) string {
	trimmed := strings.Trim(name, " ")
	lowered := strings.ToLower(trimmed)
	filtered := lowerAlphaRegex.ReplaceAllString(lowered, "")

	return filtered
}
