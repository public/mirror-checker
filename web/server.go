package web

import "github.com/gofiber/fiber/v2"

func StartServer() error {
	app := fiber.New()

	// TODO: authentication middleware? is it needed?

	app.Get("/", func(c *fiber.Ctx) error {
		return c.SendString("Hi! Why are you here? :)")
	})
	app.Get("/health", getHealth)
	app.Get("/healthz", getHealth)
	app.Get("/alive", getHealth)

	app.Get("/status", getStatus)

	app.Get("/api/info", getInfoGlobal)
	app.Get("/api/status", getStatusGlobal)
	app.Get("/api/project/:proj/info", getProjectInfo)
	app.Get("/api/project/:proj/status", getProjectStatus)
	// TODO: initiate re-checks?

	app.Listen(":4200") // TODO: custom port and address

	return nil
}

// "/api/info"
// similar to "/api/project/:proj/info" but only all projects
func getInfoGlobal(c *fiber.Ctx) error {
	// TODO: implement
	// TODO: what is this for??
	return c.SendStatus(200)
}

// "/api/status"
// similar to "/api/project/:proj/status" but only all projects
func getStatusGlobal(c *fiber.Ctx) error {
	// check all known projects for errors

	return c.SendStatus(200)
}

// "/api/project/:proj/info"
// returns: detailed information about a project's status and configuration
func getProjectInfo(c *fiber.Ctx) error {
	// TODO: implement
	return c.SendStatus(200)
}

// "/api/project/:proj/status"
// returns: 200 code if no errors are found
// returns: 400 code if errors are found in any checkers
// returns: 500 code if the server experienced an error
func getProjectStatus(c *fiber.Ctx) error {
	// TODO: implement
	return c.SendStatus(200)
}

////////////////////////////////////////////////////////////////////////////////////////////////////
// API matrics

func getHealth(c *fiber.Ctx) error {
	return c.SendStatus(200)
}

func getStatus(c *fiber.Ctx) error {
	// TODO: implement
	return c.SendStatus(200)
}
